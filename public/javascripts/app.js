(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return ({}).hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';

    if (has(cache, path)) return cache[path].exports;
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex].exports;
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  var define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  var list = function() {
    var result = [];
    for (var item in modules) {
      if (has(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  globals.require = require;
  globals.require.define = define;
  globals.require.register = define;
  globals.require.list = list;
  globals.require.brunch = true;
})();
require.register("application", function(exports, require, module) {
(function() {
  var Application, R;

  R = require("lib/core");

  Application = {
    initialize: function() {
      R.map("map", {
        tiles: ["http://tileserver.mystand.ru/{z}/{y}/{x}.png"]
      });
      return console.log("test ristow app initialized");
    }
  };

  module.exports = Application;

}).call(this);

});

;require.register("initialize", function(exports, require, module) {
(function() {
  var application;

  application = require('application');

  $(function() {
    return application.initialize();
  });

}).call(this);

});

;require.register("lib/core", function(exports, require, module) {
(function() {
  var R, RistowMap,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  R = {
    map: function() {
      return (function(func, args, ctor) {
        ctor.prototype = func.prototype;
        var child = new ctor, result = func.apply(child, args);
        return typeof result === "object" ? result : child;
      })(RistowMap, arguments, function() {});
    }
  };

  RistowMap = (function() {

    function RistowMap(selector, opts) {
      this.render = __bind(this.render, this);      this.opts = opts;
      this.el = $("#" + selector);
      this.$canvas = $("<canvas>").appendTo(this.el);
      this.canvas = this.$canvas.get(0);
      this.tile = {
        width: 256,
        height: 256
      };
      this.tiles = opts.tiles || [];
      _.defer(this.render);
    }

    RistowMap.prototype.render = function() {
      var defaultCoords;
      this.width = this.$canvas.width();
      this.height = this.$canvas.height();
      this.canvas.width = this.width;
      this.canvas.height = this.height;
      this.ctx = this.canvas.getContext('2d');
      defaultCoords = {
        z: 0,
        x: 0,
        y: 0
      };
      return this.renderTile(this.tiles[0], defaultCoords);
    };

    RistowMap.prototype.renderTile = function(tile, opts) {
      var img,
        _this = this;
      img = document.createElement("img");
      img.id = "" + opts.z + "-" + opts.x + "-" + opts.y;
      img.src = this._getTilePath(tile, opts);
      img.height = this.tile.height;
      img.width = this.tile.width;
      return img.onload = function() {
        return _this.ctx.drawImage(img, 0, 0, _this.width, _this.height);
      };
    };

    RistowMap.prototype._getTilePath = function(tile, opts) {
      var key, regexp, val;
      for (key in opts) {
        val = opts[key];
        regexp = new RegExp("\{" + key + "\}");
        tile = tile.replace(regexp, val);
      }
      return tile;
    };

    return RistowMap;

  })();

  module.exports = R;

}).call(this);

});

;
//# sourceMappingURL=app.js.map