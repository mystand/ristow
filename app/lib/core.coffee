R =
  map: ->
    new RistowMap arguments...

class RistowMap
  constructor: (selector, opts) ->
    @opts = opts
    @el = $ "#" + selector
    @$canvas = $("<canvas>").appendTo @el
    @canvas = @$canvas.get 0
    @tile =
      width: 256
      height: 256

    @tiles = opts.tiles || []
    _.defer @render

  render: =>
    @width = @$canvas.width()
    @height = @$canvas.height()
    @canvas.width = @width
    @canvas.height = @height

    @ctx = @canvas.getContext '2d'

    defaultCoords =
      z: 0
      x: 0
      y: 0

    @renderTile @tiles[0], defaultCoords

  renderTile: (tile, opts) ->
    img = document.createElement "img"
    img.id = "#{opts.z}-#{opts.x}-#{opts.y}"
    img.src = @_getTilePath tile, opts
    img.height = @tile.height
    img.width = @tile.width
    img.onload = =>
      @ctx.drawImage(img, 0, 0, @width, @height)

  _getTilePath: (tile, opts) ->
    for key, val of opts
      regexp = new RegExp("\{#{key}\}")
      tile = tile.replace regexp, val
    tile


module.exports = R